#include "Module.h"
#include <iostream>
#include <iomanip>
#include <ctime>
using namespace std;

void Task::StringIntoTime(char * duration)
{
	hours = 0;
	minutes = 0;
	for (short i = 0; i < 2; i++)
	{
		hours = hours * 10 + (duration[i] - 48);
		minutes = minutes * 10 + (duration[i + 3] - 48);
	}
	if (minutes >= 60)
	{
		hours++;
		minutes -= 60;
	}
}

void Task::TimeIntoRealHours()
{
	realHours = hours + (minutes / 60.0f);
}

void Task::TimeIntoMinutesAtAll()
{
	minutesAtAll = hours * 60 + minutes;
}

void Employeer::OutputGreatTasks(float durationInHours)
{
	for (int i = 0; i < taskNum; i++)
	{
		if (tasks[i].realHours > durationInHours)
		{
			cout << tasks[i].ID << ": " << setprecision(2) << fixed << tasks[i].realHours << endl;
		}
	}
}

void Employeer::EnterData()
{
	cout << "Enter tasks number: ";
	cin >> taskNum;
	cout << endl;
	tasks = new Task[taskNum];
	char* durationTask = new char[MaxLength + 1];
	cin.getline(durationTask, 1);
	bool inputIsGood;
	for (short i = 0; i < taskNum; i++)
	{
		inputIsGood = true;
		cout << "Enter duration task " << i + 1 << " in 'HH:MM' format: " << endl;
		cin.getline(durationTask, 1000);
		if (strlen(durationTask) == MaxLength)
		{
			if (durationTask[2] != ':')
			{
				inputIsGood = false;
			}
			else for (short j = 0; j < MaxLength; j++)
			{
				if (j != 2)
				{
					if (!isdigit(durationTask[j]))
					{
						inputIsGood = false;
						break;
					}
				}
			}
		}
		else
		{
			inputIsGood = false;
		}
		if (!inputIsGood)
		{
			i--;
			cout << "Wrong format. Use 'HH:MM'" << endl;
		}
		else
		{
			tasks[i] = Task(durationTask, i + 1);
		}
	}
}

void Employeer::FillRandom()
{
	cout << "Enter tasks number: ";
	cin >> taskNum;
	cout << endl;
	tasks = new Task[taskNum];
	srand(time(nullptr));
	char digit[MaxLength + 1]{ 0 };
	digit[2] = ':';
	for (short j = 0; j < taskNum; j++)
	{
		for (short i = 0; i < 5; i++)
		{
			if (i != 2)
			{
				if (i == 1 && digit[0] == '0')
				{
					digit[i] = rand() % 6 + 4 + 48;
				}
				else
				{
					digit[i] = rand() % 10 + 48;
				}
			}
		}
		tasks[j] = Task(digit, j + 1);
		cout << "Duration of " << j + 1 << " task:" << endl;
		cout << digit << endl;
	}

}

int Employeer::CalcDuration(float durationInHours)
{
	int sum = 0;
	int result;
	for (int i = 0; i < taskNum; i++)
	{
		sum += tasks[i].minutesAtAll;
	}

	result = sum / int(durationInHours * 60);
	cout << "sum of real hours: " << endl;
	cout << sum/60.0f << endl;
	if (sum % int(durationInHours * 60) > 0)
	{
		result++;
	}
	return result;
}
