#pragma once

const short MaxLength = 5;

struct Task
{
	Task(char* i_duration = "00:00", int i_ID = 1) :
		ID(i_ID)
	{
		StringIntoTime(i_duration);
		TimeIntoMinutesAtAll();
		TimeIntoRealHours();
	}

	int ID;
	short hours;
	short minutes;
	float realHours;
	unsigned int minutesAtAll;

	void StringIntoTime(char* duration);
	void TimeIntoRealHours();
	void TimeIntoMinutesAtAll();
};

struct Employeer
{
	Employeer(bool isManualInput = false)
	{	
		if (isManualInput)
		{
			EnterData();
		}
		else
		{
			FillRandom();
		}
	}

	int taskNum;
	Task* tasks;
	void EnterData();
	void FillRandom();
	int CalcDuration(float durationInHours);
	void OutputGreatTasks(float durationInHours);
};

